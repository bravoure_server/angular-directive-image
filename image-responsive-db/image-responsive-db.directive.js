
(function (){
    'use strict';

    function imageResponsiveDb (PATH_CONFIG, alternateSizes) {

        return {
            restrict: "E",
            replace: true,
            scope: {
                common: '=',
                module: '=',
                image: '=',
                filterName: '='
            },

            link: function(scope, element, attrs) {
                scope.attrs = attrs;
                scope.imageLoaderReloaded = false;

                // Check if the image sizes needed and the ones provided in the alternate_size API information match
                alternateSizes (scope, element);

            },

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-directive-image/image-responsive-db/image-responsive-db.html'
        };

    }

    imageResponsiveDb.$inject = ['PATH_CONFIG', 'alternateSizes'];

    angular
        .module('bravoureAngularApp')
        .directive('imageResponsiveDb', imageResponsiveDb);
})();



