# Bravoure - Image Responsive Static v.1.0

## Directive: Code to be added:


    <image_responsive_db
            filter_name=""    
            image=""            
            common=""       
            class=""              
            img_class=""              
            alt="">               
    </image_responsive_static>

          
***filtername***: setted up in src/app/config/image_size.js
***image***: the image url
***common***: boolean - used for common images through devicesthe image url
***class***: css class to be added to the container of the image
***img_class***: css class to be added to the image
***alt***: alt text to add to the image
