
(function (){
    'use strict';

    function imageResponsive (PATH_CONFIG) {

        return {
            restrict: "EA",
            replace: false,
            transclude: false,
            scope: {},
            link: function(scope, element, attrs) {
                scope.attrs = attrs;
            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-directive-image/image-responsive-static/image-responsive-static.html'
        };

    }

    imageResponsive.$inject = ['PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .directive('imageResponsiveStatic', imageResponsive);
})();

