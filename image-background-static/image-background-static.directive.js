(function () {
    'use strict';

    function imageBackground(PATH_CONFIG) {

        return {
            restrict: "E",
            replace: false,
            scope: {},
            link: function (scope, element, attrs) {
                scope.imageBackground = attrs;
            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-directive-image/image-background-static/image-background-static.html'
        };

    }

    imageBackground.$inject = ['PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .directive('imageBackgroundStatic', imageBackground);
})();

