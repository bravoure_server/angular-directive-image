(function () {

    'use strict';

    function imageBackgroundDb (PATH_CONFIG, alternateSizes) {

        return {
            restrict: "E",
            replace: true,
            scope: {
                common: '=',
                module: '=',
                image: '=',
                filterName: '='
            },
            link: function (scope, element, attrs) {
                scope.attrs = attrs;

                scope.attrs.preload = (typeof scope.attrs.preload != 'undefined') ? true : false;

                $(window).resize(function () {
                    element.find('.lazypreload').remove();
                });

                scope.$watch('imageClass', function () {
                });

                // Check if the image sizes needed and the ones provided in the alternate_size API information match
                alternateSizes(scope, element);

            },

            controller: function ($scope, $controller) {
                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-directive-image/image-background-db/image-background-db.html'
        };

    }

    imageBackgroundDb.$inject = ['PATH_CONFIG', 'alternateSizes'];

    angular
        .module('bravoureAngularApp')
        .directive('imageBackgroundDb', imageBackgroundDb)
        .filter('num', function () {
            return function (input) {
                return parseInt(input, 10);
            };
        });

})();
